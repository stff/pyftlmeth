from .AutoCorrelation import AutoCorrelation
from .FourierTransform import FourierTransform

from .TimeSeries import TimeSeries


class PowerSpectralDensity():
    def __init__(self, size=128, dtype="float64"):
        """
        Returns a callable Power Spectral Density object.

        size:       specifies the array sizes for the input array for which
                    PSD shall be calculated.
        dtype:      specifies input data type. Default float64.
        """
        self.input_array_size = size
        self.data_type = dtype
        self.AutoCorr = AutoCorrelation(self.input_array_size, self.data_type)
        self.FFT = FourierTransform(self.AutoCorr.correlation.output_array_size, self.data_type)
        self.output_array_size = self.FFT.output_array_size

    def __call__(self, time_series):
        if not isinstance(time_series, TimeSeries):
            raise TypeError("Input data must be of type pyftlmeth.TimeSeries.")

        if time_series.data.size != self.input_array_size:
            raise ValueError(f"Time series must be of length {self.input_array_size}")

        ac = self.AutoCorr(time_series)
        psd = self.FFT(ac)

        return psd
