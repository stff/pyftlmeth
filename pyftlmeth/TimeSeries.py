import numpy as np

from .TransformResult import TransformResult


class TimeSeries(TransformResult):
    def __init__(self, data, fs):
        self._data = data
        # sampling frequency in Hz
        self._fs = float(fs)
        self._update_x_axis()

    def __str__(self):
        return "Timeseries"

    def _update_x_axis(self):
        self._x_axis = np.linspace(
            start=0.0,
            stop=self._data.size / self.fs,
            num=self._data.size
        )

    @property
    def data(self):
        """Returns the y axis of the time series."""
        return self._data

    @property
    def time(self):
        """Returns the x axis of the time series."""
        return self._x_axis

    @property
    def fs(self):
        """Returns the sampling frequency of the time series."""
        return self._fs

    # Mathematical operations on time series
    def __add__(self, other):
        if not np.array_equal(self.time, other.time):
            raise ValueError("Cannot add TimeSeries: time axes not equal.")
        return TimeSeries(np.add(self.data, other.data), fs=self.fs)

    def __sub__(self, other):
        if not np.array_equal(self.time, other.time):
            raise ValueError("Cannot subtract TimeSeries: time axes not equal.")
        return TimeSeries(np.subtract(self.data, other.data), fs=self.fs)

    def __mul__(self, other):
        if not np.array_equal(self.time, other.time):
            raise ValueError("Cannot multiply TimeSeries: time axes not equal.")
        return TimeSeries(np.multiply(self.data, other.data), fs=self.fs)

    # def __div__(self, other):
    #     pass

    @property
    def conj(self):
        return TimeSeries(np.conj(self._data), fs=self.fs)
