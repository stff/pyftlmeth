import pytest


def test_TimeSeries_add(time_series):
    ts1 = time_series(5, 2000, frequency=20.0)
    ts2 = time_series(5, 2000, frequency=50.0)
    ts1 + ts2


def test_TimeSeries_add_fail(time_series):
    ts1 = time_series(5, 2000, frequency=20.0)
    ts2 = time_series(5, 1700, frequency=50.0)
    with pytest.raises(ValueError):
        ts1 + ts2


def test_TimeSeries_subtract(time_series):
    ts1 = time_series(5, 2000, frequency=20.0)
    ts2 = time_series(5, 2000, frequency=50.0)
    ts1 - ts2


def test_TimeSeries_subtract_fail(time_series):
    ts1 = time_series(5, 2000, frequency=20.0)
    ts2 = time_series(5, 1700, frequency=50.0)
    with pytest.raises(ValueError):
        ts1 - ts2


def test_TimeSeries_multiply(time_series):
    ts1 = time_series(5, 2000, frequency=20.0)
    ts2 = time_series(5, 2000, frequency=50.0)
    ts1 * ts2


def test_TimeSeries_multiply_fail(time_series):
    ts1 = time_series(5, 2000, frequency=20.0)
    ts2 = time_series(5, 1700, frequency=50.0)
    with pytest.raises(ValueError):
        ts1 * ts2
