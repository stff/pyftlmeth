import numpy as np
import random
from pyftlmeth import (
    FourierTransform,
    InverseFourierTransform
)

from .util import fft_reference


def test_Fourier_Transform(time_series):
    duration = random.randint(5, 10)
    num_points = random.randint(200 * duration, 2000 * duration)
    ts = time_series(duration, num_points)
    FFT = FourierTransform(
        size=len(ts.data),
        dtype=str(ts.data.dtype)
    )
    result = FFT(ts)
    expected = fft_reference(ts.data)
    assert np.allclose(result.data, expected)


def test_Fourier_Transform_noisy_data(time_series_noisy):
    duration = random.randint(5, 10)
    num_points = random.randint(200 * duration, 2000 * duration)
    ts = time_series_noisy(duration, num_points)
    FFT = FourierTransform(
        size=len(ts.data),
        dtype=str(ts.data.dtype)
    )
    result = FFT(ts)
    expected = fft_reference(ts.data)
    assert np.allclose(result.data, expected)


def test_FFT_then_IFFT(time_series):
    duration = random.randint(5, 10)
    num_points = random.randint(200 * duration, 2000 * duration)
    ts = time_series(duration, num_points)
    FFT = FourierTransform(
        size=len(ts.data),
        dtype=str(ts.data.dtype)
    )
    IFFT = InverseFourierTransform(
        size=FFT.output_array_size,
        dtype=FFT.output_data_type
    )
    spectrum = FFT(ts)
    result = IFFT(spectrum)
    assert np.allclose(result.data, ts.data)
