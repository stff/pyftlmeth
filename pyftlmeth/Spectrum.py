import numpy as np

from .TransformResult import TransformResult


class Spectrum(TransformResult):
    def __init__(self, data, fs):
        self._data = data
        self._fs = fs
        self._update_x_axis()

    def __str__(self):
        return "Spectrum"

    def _update_x_axis(self):
        self._x_axis = np.linspace(0.0, self.fs / 2.0, self._data.size)

    @property
    def data(self):
        """Returns the y axis of the spectrum."""
        return self._data

    @property
    def freq(self):
        """Returns the spectrum's frequency axis."""
        return self._x_axis

    @property
    def fs(self):
        """Returns the sampling frequency of the spectrum."""
        return self._fs

    def __add__(self, other):
        if not np.array_equal(self.freq, other.freq):
            raise ValueError("Cannot add Spectrum: frequency axes not equal.")
        return Spectrum(np.add(self.data, other.data), fs=self.fs)

    def __sub__(self, other):
        if not np.array_equal(self.freq, other.freq):
            raise ValueError("Cannot subtract Spectrum: frequency axes not equal.")
        return Spectrum(np.subtract(self.data, other.data), fs=self.fs)

    def __mul__(self, other):
        if not np.array_equal(self.freq, other.freq):
            raise ValueError("Cannot multiply Spectrum: frequency axes not equal.")
        return Spectrum(np.multiply(self.data, other.data), fs=self.fs)

    # def __div__(self, other):
    #     pass

    @property
    def conj(self):
        return Spectrum(np.conj(self._data), fs=self._fs)
