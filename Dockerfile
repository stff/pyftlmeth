FROM python:3.9-buster
RUN apt-get update

# install libFFTW
RUN apt-get install -y fftw3-dev


RUN python -m pip install --user --upgrade pip
RUN pip install ipython matplotlib

# install test requirements
COPY tests/requirements.txt /code/tests/requirements.txt
RUN pip install -r /code/tests/requirements.txt

# install pyftlmeth
COPY pyftlmeth /code/pyftlmeth
COPY tests /code/tests
COPY .git /code/.git
COPY README.md /code
COPY setup.py /code

# run tests
WORKDIR /code
RUN python /code/setup.py install
