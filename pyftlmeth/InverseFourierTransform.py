import numpy as np
import pyfftw

from .FourierMethodBase import FourierMethodBase
from .TimeSeries import TimeSeries
from .Spectrum import Spectrum


class InverseFourierTransform(FourierMethodBase):
    def __init__(self, size=128, dtype="complex128", real_out=True):
        """
        Returns a callable Inverse Fourier Transform object.

        size:    specify the array sizes for the two input arrays to
                 be cross correlated.
        dtype:   specify numpy input data type. Default complex128.
        real_out: specify whether the output of IFFT is
                 real or complex valued. Default is true = real output.
        """
        super().__init__()

        if size < 1:
            raise ValueError("Array size must be >= 1.")

        self.input_array_size = int(size)
        self.input_data_type = dtype
        self.real_out = real_out
        self.num_threads = self.floorLog2(self.input_array_size)

        if self.input_data_type == "complex64" and self.real_out is True:
            self.output_data_type = "float32"
            self.output_array_size = (self.input_array_size - 1) * 2
        elif self.input_data_type == "complex128" and self.real_out is True:
            self.output_data_type = "float64"
            self.output_array_size = (self.input_array_size - 1) * 2
        elif self.input_data_type == "clongdouble" and self.real_out is True:
            self.output_data_type = "longdouble"
            self.output_array_size = (self.input_array_size - 1) * 2
        elif self.input_data_type == "complex64" and self.real_out is False:
            self.output_data_type = "complex64"
            self.output_array_size = self.input_array_size
        elif self.input_data_type == "complex128" and self.real_out is False:
            self.output_data_type = "complex128"
            self.output_array_size = self.input_array_size
        else:
            raise ValueError("Invalid input data type or invalid combination.")

        self.input_array = pyfftw.zeros_aligned(self.input_array_size, dtype=self.input_data_type)
        self.output_array = pyfftw.zeros_aligned(self.output_array_size, dtype=self.output_data_type)

        self.load_wisdom()

        self.IFFT = pyfftw.FFTW(
            self.input_array,
            self.output_array,
            direction="FFTW_BACKWARD",
            threads=self.num_threads
        )
        # calling the IFFT object to initiate FFTW plan for transformation
        self.IFFT()
        self.save_wisdom()

    def __call__(self, spectrum):
        """Computes the inverse fourier transform of the input time series data(t)."""
        if not isinstance(spectrum, Spectrum):
            raise TypeError("input must be of type pyftlmeth.Spectrum")

        data = spectrum.data
        if data.ndim > 1:
            raise ValueError("Input data must be one dimentional.")
        if data.dtype != self.input_array.dtype:
            raise TypeError(f"Input data must be an array of type '{self.input_array.dtype}'")
        if data.size != self.input_array.size:
            raise ValueError(f"Input data array must have a length {self.input_array.size}")

        self.input_array[:] = np.copy(np.array(data, dtype=self.input_data_type))
        self.IFFT.update_arrays(self.input_array, self.output_array)
        self.IFFT.execute()

        return TimeSeries(self.output_array / 2.0, spectrum.fs)

    def __str__(self):
        return f"Inverse Fourier Transform\n \
                Input array size:  {self.input_array_size}\n \
                Input data type:   {self.input_data_type}\n \
                Output array size: {self.output_array_size}\n \
                Output data type:  {self.output_data_type}"
