

# ## ==============================================================================
# class Deconvolution():
#     ## --------------------------------------------------------------------------
#     def __init__(self, arraySize=128, dtype='float64'):
#         self.FFT  = FourierTransform(arraySize, dtype)
#         self.IFFT = InverseFourierTransform(arraySize, dtype)

#     ## --------------------------------------------------------------------------
#     def __del__(self):
#         pass

#     ## --------------------------------------------------------------------------
#     def __call__(self, h, g):
#         """Deconvolves the recorded time series h(t) which was previously
#            convolved with a signal g(t).

#            Returns the function f(t), which satifies f * g = h.

#            The used method is fast fourier transform: f(t) = iFFT(H/G).
#         """
#         #self.__checkData(h.ndim, g.ndim, h.shape, g.shape)

#         H = self.FFT(h)
#         G = self.FFT(g)

#         F = np.divide(H, G)
#         f = self.IFFT(F)

#         return f

#     ## --------------------------------------------------------------------------
#     def __str__(self):
#         return "Deconvolution"
