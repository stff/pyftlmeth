import pytest


def test_Spectrum_add(spectrum):
    s1 = spectrum(40, 20, 1000, 10000)
    s2 = spectrum(100, 10, 1000, 10000)
    s1 + s2


def test_Spectrum_add_fail(spectrum):
    s1 = spectrum(40, 20, 1000, 10000)
    s2 = spectrum(100, 10, 3000, 10000)
    with pytest.raises(ValueError):
        s1 + s2


def test_Spectrum_subtract(spectrum):
    s1 = spectrum(40, 20, 1000, 10000)
    s2 = spectrum(100, 10, 1000, 10000)
    s1 - s2


def test_Spectrum_subtract_fail(spectrum):
    s1 = spectrum(40, 20, 1000, 10000)
    s2 = spectrum(100, 10, 3000, 10000)
    with pytest.raises(ValueError):
        s1 - s2


def test_Spectrum_multiply(spectrum):
    s1 = spectrum(40, 20, 1000, 10000)
    s2 = spectrum(100, 10, 1000, 10000)
    s1 * s2


def test_Spectrum_multiply_fail(spectrum):
    s1 = spectrum(40, 20, 1000, 10000)
    s2 = spectrum(100, 10, 3000, 10000)
    with pytest.raises(ValueError):
        s1 * s2
