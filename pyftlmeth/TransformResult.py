
import numpy as np


class TransformResult():
    @property
    def magnitude(self):
        """Returns the magnitude of the data."""
        return np.abs(self.data)

    @property
    def phase(self):
        """Returns the phase of the data."""
        return np.arctan2(self.real, self.imag)

    @property
    def real(self):
        """Returns the real part of the data."""
        return np.real(self.data)

    @property
    def imag(self):
        """Returns the imaginary part of the data."""
        return np.imag(self.data)

    @property
    def data(self):
        """Returns the data array."""
        return self._data

    @data.setter
    def data(self, value):
        """Sets a complex data array."""
        # if value.dtype not in np.sctypes['complex']:
        #     raise TypeError("Data must be of type complex.")
        self._data = np.copy(value)

    def set_magnitude_phase(self, magnitude, phase):
        """
        Sets the internal data by magnitude and phase.
        magnitude and phase must be one-dimensional and of same size.

        Parameters
        ----------
        magnitude : array_like, one-dimensional
                    The data-type of magnitude must be float.
        phase : array_like, one-dimensional
                The data-type of phase must be float.
        """
        if magnitude.size != phase.size:
            raise ValueError("Magnitude and phase part must be of same size.")

        # complexDataType = self._check_data_types(magnitude.dtype, phase.dtype)

        real = np.multiply(np.cos(phase), magnitude)
        imag = np.multiply(np.sin(phase), magnitude)

        self.set_real_imag(real, imag)

    def set_real_imag(self, real, imag):
        """
        Sets the internal data by real and imaginary parts.
        real and imag must be one-dimensional and of same size.

        Parameters
        ----------
        real : array_like, one-dimensional
               The data type of real must be float.
        imag : array_like, one-dimensional
               The data type of imag must be float.
        """
        if real.size != imag.size:
            raise ValueError("Real and imaginary part must be of same size.")

        complexDataType = self._check_data_types(real.dtype, imag.dtype)
        self._data = np.empty(real.size, dtype=complexDataType)
        self._data[:] = real + 1j * imag

    def _check_data_types(self, dtypeA, dtypeB):
        if dtypeA not in np.sctypes['float']:
            raise ValueError("Real data must be of type float.")

        if dtypeB not in np.sctypes['float']:
            raise ValueError("Imaginary data must be of type float.")

        if dtypeA != dtypeB:
            raise ValueError("Real and imaginary part must be of same dtype.")

        if dtypeA == np.float32:
            complexDataType = np.complex64
        elif dtypeA == np.float64:
            complexDataType = np.complex128
        else:
            raise ValueError("Invalid input data type or invalid combination.")

        return complexDataType
