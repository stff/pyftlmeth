from .CrossCorrelation import CrossCorrelation


class AutoCorrelation():
    def __init__(self, size=128, dtype="float64", real_out=True):
        """
        Returns a callable Auto-Correlation object.

        size:    specifies the array sizes for the two input arrays to
                 be cross correlated.
        dtype:   specifies input data type. Default float64.
        real_out: specifies whether the output of cross correlation is
                 real or complex data type. Default true.
        """
        # This is implemented via Cross Correlation
        self.correlation = CrossCorrelation(size=size, dtype=dtype, real_out=real_out)

    def __call__(self, data):
        """Computes the autocorrelation r(tau) of the input time series x(t).

           The method is using fast fourier transform with
           F(f) = FFT[x(t)]
           S(f) = F(f) * c.c.[F(f)]
           r(tau) = IFFT[S(f)],
           where c.c. denotes complex conjugated.
        """
        return self.correlation(data, data)

    def __str__(self):
        return f"Autocorrelation\n \
                Input array size:  {self.correlation.FFT.inputArraySize}\n \
                Input data type:   {self.correlation.FFT.inputDataType}\n \
                Output array size: {self.correlation.IFFT.outputArraySize}\n \
                Output data type:  {self.correlation.IFFT.outputDataType}"
