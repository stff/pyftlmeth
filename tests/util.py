import numpy as np


def fft_reference(data):
    """Using numpy's FFT implementation as a reference"""
    spectrum = np.fft.fft(data)
    # normalize using Parsevals theorem
    N = spectrum.size
    spectrum = (2 / N) * spectrum
    # cut mirrored spectrum
    spectrum = spectrum[:spectrum.size // 2 + 1]
    return spectrum
