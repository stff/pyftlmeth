import numpy as np
import pytest

from pyftlmeth import (
    TimeSeries,
    Spectrum
)


@pytest.fixture
def cosine_data():
    def _factory(duration, num_points, frequency=1.0):
        time = np.linspace(0, duration, num_points)
        data = np.cos(2 * np.pi * frequency * time)
        return data
    return _factory


@pytest.fixture
def lorentzian_data():
    def _factory(center, fwhm, fs, num_points):
        if center + 0.5 * fwhm >= fs / 2.0:
            raise ValueError("Lorantzian out of frequency range.")
        freq = np.linspace(0.0, fs / 2.0, num_points)
        return fwhm / (2 * np.pi) / (np.square(freq - center) + np.square(0.5 * fwhm))
    return _factory


@pytest.fixture
def time_series(cosine_data):
    def _factory(duration, num_points, frequency=1.0):
        data = cosine_data(duration, num_points, frequency)
        return TimeSeries(
            data=data,
            fs=duration / num_points
        )
    return _factory


@pytest.fixture
def time_series_noisy(cosine_data):
    def _factory(duration, num_points, frequency=1.0):
        data = cosine_data(duration, num_points, frequency)
        gaussian_noise = np.random.normal(size=data.size)
        data += gaussian_noise
        return TimeSeries(
            data=data,
            fs=duration / num_points
        )
    return _factory


@pytest.fixture
def spectrum(lorentzian_data):
    def _factory(center, fwhm, fs, num_points):
        data = lorentzian_data(center, fwhm, fs, num_points)
        return Spectrum(
            data=data,
            fs=fs
        )
    return _factory
