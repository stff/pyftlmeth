import numpy as np
from pyftlmeth import TimeSeries, AutoCorrelation
import matplotlib.pyplot as plt


# test auto correlation
corr_data = np.zeros(10000)
mean = 20
corr_data_noisy = corr_data + np.random.poisson(mean, corr_data.size)
corr_data_noisy = np.roll(corr_data_noisy, np.random.randint(0, corr_data_noisy.size))
corr_data_noisy += np.roll(corr_data_noisy, np.random.randint(0, corr_data_noisy.size))

ts = TimeSeries(corr_data_noisy, fs=1.0)

autocorr = AutoCorrelation(size=ts.data.size, dtype=ts.data.dtype.name)

result = autocorr(ts)
np_result = np.correlate(ts.data, ts.data, 'full')

plt.plot(ts.time, ts.data)
plt.plot(result.time[result.time.size // 2:], result.data[result.data.size // 2:])
plt.plot(result.time[result.time.size // 2:], np_result[np_result.size // 2:])
plt.title("Autocorrelation")
plt.show()
