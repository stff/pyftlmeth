from pyftlmeth import TimeSeries, CrossCorrelation
import numpy as np
import matplotlib.pyplot as plt


n_points = int(10000)


data_a = np.zeros(n_points)
data_b = np.zeros(n_points)


start = 400
stop = 600
data_type = 'float64'
data_a[start:stop] = np.ones(stop - start, dtype=data_type)
data_b[start:stop] = np.linspace(1.0, 0.0, stop - start, dtype=data_type)


data_a = TimeSeries(data_a, fs=1.0)
data_b = TimeSeries(np.roll(data_b, np.random.randint(0, data_b.size)), fs=1.0)


# test cross correlation
corr = CrossCorrelation(size=data_a.data.size, dtype=data_a.data.dtype.name)

result = corr(data_a, data_b)
np_result = np.correlate(data_a.data, data_b.data, 'full')

plt.plot(data_a.time, data_a.data)
plt.plot(data_b.time, data_b.data)
plt.plot(result.time, result.data)
plt.plot(result.time, np_result)
plt.title("cross")
plt.show()
