# pyftlmeth
A python library for fast Fourier transformational methods based on FFTW and pyfftw.

## Usage
TBD

## Run tests locally
To run the tests locally execute
```bash
IMAGE=pyftlmeth:test
docker build --rm --tag $IMAGE -f Dockerfile .
docker run $IMAGE python -m pytest -x --cov=pyftlmeth tests/
```

## Play with the examples
You can enter an interactive playground environment in a Docker container. The folloring commands
will build the Docker container, start it and enter an `ipython` inside the volume mounted `examples`
directory. The `docker run` command enables an X display pass-through so you can directly see some nice
`matplotlib` plots.
```bash
IMAGE=pyftlmeth:test
USER=$(whoami)

docker build -t $IMAGE -f Dockerfile .

docker run --rm -it \
    --user=$(id -u) \
    --env="DISPLAY" \
    --workdir=/code/examples \
    --volume="/etc/group:/etc/group:ro" \
    --volume="/etc/passwd:/etc/passwd:ro" \
    --volume="/etc/shadow:/etc/shadow:ro" \
    --volume="/etc/sudoers.d:/etc/sudoers.d:ro" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume="/home/$USER:/home/$USER:rw" \
    --volume="$PWD/examples:/code/examples/:rw" \
    $IMAGE /bin/bash -c "ipython" \
    \
```