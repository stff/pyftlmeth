import numpy as np

from .FourierTransform import FourierTransform
from .InverseFourierTransform import InverseFourierTransform
from .TimeSeries import TimeSeries


class CrossCorrelation():
    def __init__(self, size=128, dtype="float64", real_out=True):
        """
        Returns a callable Cross Correlation object.

        size:    specify the array sizes for the two input arrays to
                 be cross correlated.
        dtype:   specify input data type. Default float64.
        real_out: specify whether the output of cross correlation is
                 real or complex. Default true.
        """
        if size < 1:
            raise ValueError("Input array size must be >= 1.")

        self.array_size = size
        self.data_type = dtype

        self.correlation_scale = int(2 * self.array_size)
        self.FFT = FourierTransform(
            self.correlation_scale,
            self.data_type
        )
        self.IFFT = InverseFourierTransform(
            size=self.FFT.output_array_size,
            dtype=self.FFT.output_data_type,
            real_out=real_out
        )
        self.output_array_size = self.IFFT.output_array_size

    def __call__(self, ts_a, ts_b):
        """Computes the crosscorrelation h(tau)
           of the input time series ts_a(t) and ts_b(t).

           The method is using fast fourier transform with
           H(f) = FFT[ts_a(t)] * c.c.(FFT[ts_b(t)])
           h(tau) = IFFT[H(f)],
           where c.c. denotes complex conjugated.
        """
        if ts_a.data.size != self.array_size or ts_b.data.size != self.array_size:
            raise ValueError(f"Data arrays must both be of length {self.array_size}")

        padded_a = TimeSeries(
            np.concatenate(
                (
                    ts_a.data,
                    np.zeros(self.correlation_scale - self.array_size, dtype=self.data_type)
                )
            ),
            fs=ts_a.fs
        )

        padded_b = TimeSeries(
            np.concatenate(
                (
                    ts_b.data,
                    np.zeros(self.correlation_scale - self.array_size, dtype=self.data_type)
                )
            ),
            fs=ts_b.fs
        )

        spectrum_a = self.FFT(padded_a)
        spectrum_b = self.FFT(padded_b)
        fft_result = spectrum_a * spectrum_b.conj

        r = self.IFFT(fft_result)
        tmp = np.split(r.data, 2)
        result = np.roll(np.concatenate((tmp[1], tmp[0])), -1)[:-1]

        return TimeSeries(result, fs=ts_a.fs)

    def __str__(self):
        return f"Cross-Correlation\nInput array size: {self.FFT.input_array_size}\n \
                                   Input data type: {self.FFT.input_data_type}  \n \
                                   Output array size: {self.IFFT.output_array_size}\n \
                                   Output data type:  {self.IFFT.output_data_type}"
