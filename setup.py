import setuptools


with open("README.md", "r") as fh:
    long_description = fh.read()


setuptools.setup(
    name="pyftlmeth",
    author="Stefan Lasse",
    author_email="stefanlasse87@gmail.com",
    description="A python library for fast Fourier transformation methods based on FFTW.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/stff/pyftlmeth",
    packages=setuptools.find_packages(
        exclude=["*.tests", "*.tests.*", "tests.*", "tests"]
    ),
    setup_requires=[
        "setuptools_scm>=5,<6",
        "cython>=0.29",
    ],
    use_scm_version=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: MIT",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        "numpy>=1.19",
        "pyfftw>=0.12",
    ],
)
