import numpy as np
import pyfftw

from .FourierMethodBase import FourierMethodBase
from .Spectrum import Spectrum


class FourierTransform(FourierMethodBase):
    def __init__(self, size=128, dtype="float64"):
        """
        Returns a callable Fourier Transform object.

        size:  specify the array sizes for the input array to
               be fourier transformed.
        dtype: specify numpy input data type. Default is float64.
        """
        super().__init__()

        if not isinstance(size, int):
            raise TypeError("size must be an integer")

        if size < 1:
            raise ValueError("size must be >= 1")

        self.input_array_size = int(size)
        self.input_data_type = dtype
        self.num_threads = self.floorLog2(self.input_array_size)

        if self.input_data_type == "complex64":
            self.output_data_type = "complex64"
            self.output_array_size = self.input_array_size
        elif self.input_data_type == "complex128":
            self.output_data_type = "complex128"
            self.output_array_size = self.input_array_size
        elif self.input_data_type == "clongdouble":
            self.output_data_type = "clongdouble"
            self.output_array_size = self.input_array_size
        elif self.input_data_type == "float32":
            self.output_data_type = "complex64"
            self.output_array_size = self.input_array_size // 2 + 1
        elif self.input_data_type == "float64":
            self.output_data_type = "complex128"
            self.output_array_size = self.input_array_size // 2 + 1
        else:
            raise ValueError(f"Invalid input data type: {self.input_data_type}")

        self.input_array = pyfftw.zeros_aligned(self.input_array_size, dtype=self.input_data_type)
        self.output_array = pyfftw.zeros_aligned(self.output_array_size, dtype=self.output_data_type)

        self.load_wisdom()

        self.FFT = pyfftw.FFTW(
            self.input_array,
            self.output_array,
            direction="FFTW_FORWARD",
            threads=self.num_threads
        )
        # calling the FFT object to initiate FFTW plan for transformation
        self.FFT()
        self.save_wisdom()

    def __call__(self, time_series):
        """Computes the fourier transform of the input TimeSeries object."""
        data = time_series.data
        if data.ndim > 1:
            raise ValueError("Input data must be one dimentional.")
        if data.dtype != self.input_array.dtype:
            raise TypeError(f"Input data must be an array of type '{self.input_array.dtype}'")
        if data.size != self.input_array.size:
            raise ValueError(f"Input data array must have a length {self.input_array.size}")

        self.input_array[:] = np.copy(np.array(data, dtype=self.input_data_type))
        self.FFT.update_arrays(self.input_array, self.output_array)
        self.FFT.execute()

        return Spectrum(2.0 * self.output_array / self.input_array_size, time_series.fs)

    def __str__(self):
        return f"Fourier Transform\nInput array size:  {self.input_array_size}\n \
                Input data type:   {self.input_data_type}\n \
                Output array size: {self.output_array_size}\n \
                Output data type:  {self.output_data_type}"
