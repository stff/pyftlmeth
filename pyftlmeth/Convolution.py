import numpy as np

from .FourierTransform import FourierTransform
from .InverseFourierTransform import InverseFourierTransform
from .Spectrum import Spectrum
from .TimeSeries import TimeSeries


class Convolution():
    def __init__(self, signal_size=128, response_size=64, dtype="float64"):
        self.signal_size = signal_size
        self.response_size = response_size
        self.dtype = dtype

        self.result_size = self.signal_size + self.response_size - 1

        self.FFT = FourierTransform(self.result_size, dtype)
        self.IFFT = InverseFourierTransform(self.FFT.output_array_size, self.FFT.output_data_type)

        self.fft_response = Spectrum(np.zeros(self.result_size, dtype=self.FFT.output_data_type))

    def __call__(self, signal):
        """Convolves the input time series f(t) and g(t) satisfying h = f * g.
           Here, f is the input data and g is the response function, which must
           be set separately.

           The used method is fast fourier transform with h(t) = IFFT(F*G),
           where F and G are the fourier tansforms of the input time series f and g.

           NOTE:
           It is assumed, that the response function will stay the same for
           many signals to be convolved with. Therefore, the response function
           must be provided separately by calling setResponseFunction(response).
           Afterwords every input signal will be convolved with this response function
           until it is changed again by setResponseFunction(newResponse). This
           method saves almost half of the computation time.
        """
        # add zero padding to signal
        padded_signal = TimeSeries(
            np.concatenate(
                (
                    signal.data,
                    np.zeros(self.result_size - self.signal_size, dtype=signal.data.dtype)
                )
            ),
            fs=signal.samplFreq
        )

        fft_signal = self.FFT(padded_signal)
        fft_result = fft_signal * self.fft_response
        result = self.IFFT(fft_result)

        return result

    def __str__(self):
        return "Convolution"

    def setResponseFunction(self, response):
        """Since the response function is assumed to be the same for many
           signals to be convolved with, it must be initialized first to
           be reused with every signal to be convolved. This saves lots
           of FFT computation time.
        """
        padded_response = TimeSeries(
            np.concatenate(
                (
                    response.data,
                    np.zeros(self.result_size - self.response_size, dtype=response.data.dtype)
                )
            ),
            fs=response.fs
        )

        self.fft_response = self.FFT(padded_response)
