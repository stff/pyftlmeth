import numpy as np

from os.path import expanduser, join
from os import makedirs

import pyfftw
import pickle


class FourierMethodBase():
    def __init__(self):
        self.pyftlmeth_homedir = ".pyftlmeth"

    def floorLog2(self, n):
        """Finds the largest power of 2 which is smaller than the array size."""
        last = n
        n &= n - 1
        while n:
            last = n
            n &= n - 1
        return int(np.log2(last))

    def get_wisdom(self):
        """Returns the current wisdom object of the fftw library."""
        return pyfftw.export_wisdom()

    def set_wisdom(self, wisdom):
        """Loads the wisdom object parameter to the fftw library."""
        pyfftw.import_wisdom(wisdom)

    def forget_wisdom(self):
        """Resets the internal FFTW scheme plan."""
        pyfftw.forget_wisdom()

    def save_wisdom(self):
        """Saves the FFTW scheme plan to a pickle file."""
        wisdom = self.get_wisdom()
        home = expanduser("~")
        directory = join(home, self.pyftlmeth_homedir)

        # check if pyfftw-directory is present in user's home directory
        makedirs(directory, exist_ok=True)

        wisdom_file = join(directory, "pyfftw_wisdom.pys")
        with open(wisdom_file, "wb") as f:
            pickle.dump(wisdom, f)

    def load_wisdom(self):
        """Loads a former saved FFTW scheme plan from a pickle file."""
        home = expanduser("~")
        pyfftw_dir = self.pyftlmeth_homedir
        # if 'win' in sys.platform:
        #    pyfftw_dir = "\\" + self.pyftlmeth_homedir + "\\"
        # else:
        #    pyfftw_dir = "~/" + self.pyftlmeth_homedir + "/"

        wisdom_file = join(home, pyfftw_dir, "pyfftw_wisdom.pys")
        try:
            with open(wisdom_file, "rb") as f:
                wisdom = pickle.load(f)
        except FileNotFoundError:
            pass
        else:
            self.set_wisdom(wisdom)
